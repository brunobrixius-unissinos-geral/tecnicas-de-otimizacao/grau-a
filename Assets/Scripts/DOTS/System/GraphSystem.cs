﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

[DisableAutoCreation]
public class GraphSystem : JobComponentSystem
{
    private EntityManager EntityManager;
    private List<Entity> _nodes = new List<Entity>();
    private NativeList<float3> _nodePosition = new NativeList<float3>(64,Allocator.Persistent);

    private int nodeCounter;
    
    private Entity _nodeEntity;

    private Entity? _initialGrahamNode = null;

    private SortedList<float,Entity> _angleSortedNodes = new SortedList<float, Entity>();
    
    #region GrahamScan

    public Action<List<Vector3>> OnHullFinish;
    #endregion
    
    private struct GrahamScan : IJob
    {
        [ReadOnly]public NativeArray<float3> SortedPositionByAngle;
        [ReadOnly] public float3 P0;

        public NativeArray<float3> hull;
        private float CrossProduct(float3 A, float3 B, float3 C)
        {
            return ((B.x - A.x) * (C.y - A.y) - (C.x - A.x) * (B.y - A.y));
        }
        public void Execute()
        {
            int lastIndex = 2; 
            hull[0] = P0;
            hull[1] = SortedPositionByAngle[0];
            for (int i = 1; i < SortedPositionByAngle.Length; i++)
            {
                float turnValue = CrossProduct(hull[lastIndex - 2], hull[lastIndex - 1], SortedPositionByAngle[i]);
                
                if (turnValue == 0)
                {
                    lastIndex--;
                    hull[lastIndex] = SortedPositionByAngle[i];
                    lastIndex++;

                }
                else if(turnValue > 0)
                {
                    hull[lastIndex] = SortedPositionByAngle[i];
                    lastIndex++;
                }
                else
                {
                    while (turnValue <= 0 && hull.Length > 2)
                    {
                        lastIndex--;
                        turnValue = CrossProduct(hull[lastIndex - 2], hull[lastIndex - 1], SortedPositionByAngle[i]);
                    }

                    hull[lastIndex] = SortedPositionByAngle[i];
                    lastIndex++;
                }
            }
        }
    }
    
//    [BurstCompile]
    private struct GraphSystemTwoJob : IJobForEachWithEntity< Translation, LabelComponent>
    {
        public void Execute(Entity entity, int index, ref Translation translationComponent, [ReadOnly]ref LabelComponent labelComponent)
        {
            Debug.Log(labelComponent.Name);
        }
    }
    [BurstCompile]
    struct GraphSystemJob : IJobForEach<NodeComponent,UniqueIDComponent,LabelComponent>
    {
        public void Execute([ReadOnly]ref NodeComponent nodeComponent, [ReadOnly]ref UniqueIDComponent idComponent, [ReadOnly]ref LabelComponent labelComponent)
        {
            Debug.Log(labelComponent.Name);
        }
    }
    
    protected override JobHandle OnUpdate(JobHandle inputDependencies)
    {
        NativeArray<float3> sortedPositions = new NativeArray<float3>(_angleSortedNodes.Count,Allocator.Persistent);
        
        NativeArray<float3> hullArray = new NativeArray<float3>(_angleSortedNodes.Count,Allocator.Persistent);
        
        int counter = 0;
        foreach (var entity in _angleSortedNodes.Values)
        {
            sortedPositions[counter] = EntityManager.GetComponentData<Translation>(entity).Value;
            counter++;
        }
        
        var grahamJob = new GrahamScan
        {
            SortedPositionByAngle = sortedPositions,
            P0 = EntityManager.GetComponentData<Translation>(_initialGrahamNode.Value).Value,
            hull = hullArray
        };
        var jobHandle = grahamJob.Schedule();

        jobHandle.Complete();
        
//        Debug.Log(grahamJob.hull.Length);
//        
//        foreach (float3 position in grahamJob.hull)
//        {
//            Debug.Log(position);
//        }
        List<Vector3> hullNodeList = new List<Vector3>();
        
        foreach (float3 position in grahamJob.hull)
        {
            if (position.Equals(float3.zero) == false)
            {
                hullNodeList.Add(position);
            }
        }
        sortedPositions.Dispose();
        hullArray.Dispose();
        
        OnHullFinish?.Invoke(hullNodeList);
        
        return jobHandle;
        
        
        var job = new GraphSystemTwoJob();

        // Now that the job is set up, schedule it to be run. 
        return job.Schedule(this, inputDependencies);
    }

    public void HandleOnSpawnNode(float3 nodePosition)
    {
        _nodePosition.Add(nodePosition);
        var newNode = World.Active.EntityManager.Instantiate(_nodeEntity);

        EntityManager.SetComponentData(newNode, new UniqueIDComponent { UniqueId = nodeCounter });
        EntityManager.SetComponentData(newNode, new LabelComponent { Name = new NativeString64(nodeCounter.ToString("X")) });
        EntityManager.SetComponentData(newNode, new Translation { Value = nodePosition });
//        Debug.Log(nodeCounter.ToString("X"));
        nodeCounter++;

        if (_initialGrahamNode == null)
        {
            _initialGrahamNode = newNode;
        }
        else
        {
            float3 P0 = EntityManager.GetComponentData<Translation>(_initialGrahamNode.Value).Value;
            float angle;
            
            if (P0.y >= nodePosition.y && P0.x > nodePosition.x)
            {
                angle = Mathf.Atan2(P0.y-nodePosition.y, P0.x-nodePosition.x) * Mathf.Rad2Deg;
                _initialGrahamNode = newNode;
            }
            else
            {
                angle = Mathf.Atan2(nodePosition.y-P0.y, nodePosition.x-P0.x) * Mathf.Rad2Deg;
            }
            Debug.Log("angle = " + angle);
            _angleSortedNodes.Add(angle,newNode);
        }
        _nodes.Add(newNode);
    }

    public void Setup()
    {
        _nodeEntity = GameObjectConversionUtility.ConvertGameObjectHierarchy(Pool.Instance.GetGameObject(PoolType.Node.Type.Node),World.Active);
    }

    protected override void OnCreate()
    {
        base.OnCreate();
        EntityManager = World.Active.EntityManager;
    }
}