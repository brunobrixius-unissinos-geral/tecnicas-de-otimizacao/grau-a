﻿using System;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Physics.Systems;
using UnityEngine;
using Ray = Unity.Physics.Ray;
using RaycastHit = Unity.Physics.RaycastHit;

//[DisableAutoCreation]
public class RayCastSystem : JobComponentSystem
{
    private BuildPhysicsWorld buildPhysicsWorld;
    const float maxDistance = 100.0f;
    private static float3? hitPosition;

    public static RayCastSystem Instance;

    public Action<float3> SpawnNode;

    //TODO Adicionar tag no Board para somente spawnar quando clicar nele
    struct PickEntityJob : IJob
    {
        [ReadOnly] public CollisionWorld CollisionWorld;
        public Ray Ray;
        public void Execute()
        {
            RigidBody? hitBody = null;

            var rayCastInput = new RaycastInput();
            rayCastInput.Start = Ray.Origin;
            rayCastInput.End = Ray.Origin + Ray.Displacement;
            rayCastInput.Filter = CollisionFilter.Default;

            if (CollisionWorld.CastRay(rayCastInput, out RaycastHit hit))
            {
                hitBody = CollisionWorld.Bodies[hit.RigidBodyIndex];
                hitPosition = hit.Position;
            }
        }
    }
    [BurstCompile]
    public struct RaycastJob : IJobParallelFor
    {
        [ReadOnly] public CollisionWorld world;
        [ReadOnly] public NativeArray<RaycastInput> inputs;
        public NativeArray<RaycastHit> results;

        public unsafe void Execute(int index)
        {
            RaycastHit hit;
            world.CastRay(inputs[index], out hit);
            results[index] = hit;
        }
    }

    public static void SingleRayCast(CollisionWorld world, RaycastInput input, ref RaycastHit result)
    {
        var rayCommands = new NativeArray<RaycastInput>(1, Allocator.TempJob);
        var rayResults = new NativeArray<RaycastHit>(1, Allocator.TempJob);
        rayCommands[0] = input;
        var handle = ScheduleBatchRayCast(world, rayCommands, rayResults);
        handle.Complete();
        result = rayResults[0];
        rayCommands.Dispose();
        rayResults.Dispose();
    }
    public static JobHandle ScheduleBatchRayCast(CollisionWorld world, NativeArray<RaycastInput> inputs, NativeArray<RaycastHit> results)
    {
        JobHandle rcj = IJobParallelForExtensions.Schedule(new RaycastJob
        {
            inputs = inputs,
            results = results,
            world = world

        }, inputs.Length, 5);
        return rcj;
    }

    protected override void OnCreate()
    {
        base.OnCreate();
        if (Instance == null)
            Instance = this;
        
        buildPhysicsWorld = World.GetOrCreateSystem<BuildPhysicsWorld>();
    }

    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        var jobHandle = JobHandle.CombineDependencies(inputDeps, buildPhysicsWorld.FinalJobHandle);

        if (Input.GetKeyDown(KeyCode.Mouse0) && (UnityEngine.Camera.main != null))
        {
            UnityEngine.Ray unityRay = UnityEngine.Camera.main.ScreenPointToRay(UnityEngine.Input.mousePosition);

            var ray = new Ray();
            ray.Displacement = unityRay.direction * maxDistance;
            ray.Origin = unityRay.origin;

            Debug.DrawRay(ray.Origin, ray.Displacement, Color.yellow);

            var rayCastInput = new RaycastInput();
            rayCastInput.Start = ray.Origin;
            rayCastInput.End = ray.Origin + ray.Displacement;
            rayCastInput.Filter = CollisionFilter.Default;

            jobHandle = new PickEntityJob
            {
                CollisionWorld = buildPhysicsWorld.PhysicsWorld.CollisionWorld,
                Ray = ray
            }.Schedule(inputDeps);
            jobHandle.Complete();
        }

        if (hitPosition != null)
        {
            SpawnNode?.Invoke(hitPosition.Value);
            hitPosition = null;
        }
        return jobHandle;
    }
}