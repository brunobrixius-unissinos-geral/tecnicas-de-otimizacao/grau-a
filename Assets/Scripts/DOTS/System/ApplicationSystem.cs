﻿using System.Collections.Generic;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.Rendering;
using static Unity.Mathematics.math;

public class ApplicationSystem : ComponentSystem
{
    private GraphSystem _graphSystem;
    protected override void OnCreate()
    {
        base.OnCreate();

    }

    protected override void OnStartRunning()
    {
        base.OnStartRunning();
        Initialize();
        SetupValues();
        SetupEvents();
    }

    private void Initialize()
    {
        _graphSystem = World.GetOrCreateSystem<GraphSystem>();
    }

    private void SetupEvents()
    {
        RayCastSystem.Instance.SpawnNode = _graphSystem.HandleOnSpawnNode;
    }

    private void SetupValues()
    {
        _graphSystem.Setup();
        _graphSystem.OnHullFinish = HandleOnHullFinish;
    }
//TODO Reset all values when pressing R
//TODO Reset all data when creating a new P0 on GranScan
    private void HandleOnHullFinish(List<Vector3> hulls)
    {
        for (var i = 1; i < hulls.Count; i++)
        {
            CreateLine(hulls[i-1],hulls[i]);
        }
        CreateLine(hulls[hulls.Count-1],hulls[0]);

    }

    private float lineWidth = 0.2f;
private void CreateLine(Vector3 startPosition, Vector3 endPosition)
    {
        var gameObject = new GameObject("Arc");
        
        var lineRenderer = gameObject.AddComponent<LineRenderer>();
//        lineRenderer.material = lineRendererMaterial;
        lineRenderer.shadowCastingMode = ShadowCastingMode.Off;
        lineRenderer.SetPositions(new[] {startPosition, endPosition});
        lineRenderer.startWidth = lineWidth;
        lineRenderer.endWidth = lineWidth;
    }
    protected override void OnUpdate()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            _graphSystem.Update();
            
        }
    }
}