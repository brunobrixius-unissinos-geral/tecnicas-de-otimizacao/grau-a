﻿using System;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;

[Serializable]
public struct LabelComponent : IComponentData
{
    public NativeString64 Name;
}