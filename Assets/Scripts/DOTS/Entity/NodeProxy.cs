﻿using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

public class NodeProxy : MonoBehaviour ,IConvertGameObjectToEntity
{
    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        dstManager.AddComponent(entity, typeof(UniqueIDComponent));
        dstManager.AddComponent(entity, typeof(LabelComponent));
        dstManager.AddComponent(entity, typeof(NodeComponent));
    }
}
