﻿using UnityEngine;

public class ApplicationStarter : MonoBehaviour
{
    public static ApplicationStarter Instance;

    [SerializeField] private GameObject nodePrefab;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(this);
        }
    }

    private void Update()
    {

    }

    public Ola GetOla()
    {
        return new Ola
        {
            ola = 10,
            nodePrefab = nodePrefab
        };
    }

    public struct Ola
    {
        public int ola;
        public GameObject nodePrefab;
    }
}