﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class Pool : MonoBehaviour
{
    private string _poolPath = "/Scripts/Pool/";
    private string _poolResources = "/Resources/Pool/";

    private int _objectCounter;

    private Dictionary<int, GameObject> _poolObjects = new Dictionary<int, GameObject>();

    public static Pool Instance;

    Stack<GameObject> _myStack;

    private FileNode _poolFileRoot = new FileNode();

    private void Awake()
    {
        Debug.Log("Awake");
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(this);
        }

        var poolGameObjects = Resources.LoadAll<GameObject>($"Pool").ToList();
        for (var i = 0; i < poolGameObjects.Count; i++)
        {
            _poolObjects.Add(i, poolGameObjects[i]);
        }
    }

    public GameObject GetGameObject<TEnum>(TEnum gameEnum)
        where TEnum : struct, IConvertible, IComparable, IFormattable
    {
        if (!typeof(TEnum).IsEnum)
        {
            throw new ArgumentException("TEnum must be an enum.");
        }
        return _poolObjects[Convert.ToInt16(gameEnum)];
    }

    public class FileNode
    {
        public string Name;
        public List<FileNode> FileNodes = new List<FileNode>();
        public List<GameObject> GameObjects;
    }

    public FileNode DirSearch(string sDir, FileNode fileNode)
    {
        try
        {
            foreach (string dir in Directory.GetDirectories(sDir))
            {
                var foundFileNode = new FileNode();
                foundFileNode.Name = dir.Split('/').Last().Split('\\').Last();
                foreach (string file in Directory.GetFiles(dir))
                {
                    if(file.Contains(".meta") == false)
                    {
                        if (foundFileNode.GameObjects == null)
                        {
                            foundFileNode.GameObjects = new List<GameObject>();
                        }
                        foundFileNode.GameObjects.Add(_myStack.Pop());
                    }
                }
                fileNode.FileNodes.Add(foundFileNode);
                DirSearch(dir, foundFileNode);
            }
        }
        catch (System.Exception excpt)
        {
            Debug.Log(excpt.Message);
        }
        return fileNode;
    }

    public void CreateEnum()
    {
        _objectCounter = 0;

        string enumFileName = "PoolType";

        var poolRootFile = Application.dataPath + _poolResources;

        string filePathAndName = "Assets/Scripts/Pool/" + enumFileName + ".cs";

        _poolFileRoot.Name = "PoolType";

        GameObject[] myGame = Resources.LoadAll<GameObject>($"Pool");

        _myStack = new Stack<GameObject>(myGame.Reverse());

        DirSearch(poolRootFile, _poolFileRoot);

        using (StreamWriter streamWriter = new StreamWriter(filePathAndName))
        {
            CreatePoolEnumRecursive(streamWriter,_poolFileRoot, 0);
        }
        AssetDatabase.Refresh();
    }

    public void CreatePoolEnumRecursive(StreamWriter streamWriter, FileNode fileNode, int layer)
    {
        for (int i = 0; i < layer; i++)
        {
            streamWriter.Write("\t");
        }
        streamWriter.WriteLine($"namespace {fileNode.Name}");

        for (int i = 0; i < layer; i++)
        {
            streamWriter.Write("\t");
        }
        streamWriter.WriteLine("{");

        if (fileNode.GameObjects != null)
        {
            for (int i = 0; i < layer+1; i++)
            {
                streamWriter.Write("\t");
            }

            streamWriter.WriteLine("public enum Type");

            for (int i = 0; i < layer + 1; i++)
            {
                streamWriter.Write("\t");
            }
            streamWriter.WriteLine("{");

            for (var i = 0; i < fileNode.GameObjects.Count; i++)
            {
                for (int j = 0; j < layer+2; j++)
                {
                    streamWriter.Write("\t");
                }
                streamWriter.WriteLine($"{fileNode.GameObjects[i].name} = {_objectCounter++},");
            }
            for (int i = 0; i < layer + 1; i++)
            {
                streamWriter.Write("\t");
            }
            streamWriter.WriteLine("}");
        }

        foreach (var node in fileNode.FileNodes)
        {
            CreatePoolEnumRecursive(streamWriter,node,layer+1);
        }
        for (int i = 0; i < layer; i++)
        {
            streamWriter.Write("\t");
        }
        streamWriter.WriteLine("}");
        streamWriter.WriteLine("");

    }

}
