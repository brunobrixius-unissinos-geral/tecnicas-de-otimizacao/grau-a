﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Pool))]
public class PoolEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        Pool myScript = (Pool)target;
        if (GUILayout.Button("Build Object"))
        {
            myScript.CreateEnum();
        }
    }
}
