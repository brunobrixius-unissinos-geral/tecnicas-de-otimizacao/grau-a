﻿using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

namespace Data
{
    public class Point
    {
        public float2 Position { get; }
    }
    public class Node
    {
        public float2 Position { get; }
    }
    public class Edge
    {
        public Node NodeA { get; }
        public Node NodeB { get; }
    }
    public class Poligon
    {
        public List<Point> Points { get; }
    }
}
