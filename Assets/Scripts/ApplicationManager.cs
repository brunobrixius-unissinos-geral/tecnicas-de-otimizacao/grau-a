﻿using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

public class ApplicationManager : MonoBehaviour
{
    public GameObject Prefab;

    // Start is called before the first frame update
    private EntityManager entityManager;
    private Entity prefab;
    void Start()
    {
        prefab = GameObjectConversionUtility.ConvertGameObjectHierarchy(Prefab, World.Active);
        entityManager = World.Active.EntityManager;
        RayCastSystem.Instance.SpawnNode += HandleSpawnEntityAt;

    }

    private void HandleSpawnEntityAt(float3 position)
    {
        var instance = entityManager.Instantiate(prefab);
        // Place the instantiated entity in a grid with some noise
        entityManager.SetComponentData(instance, new Translation { Value = position });
    }

    // Update is called once per frame
    void Update()
    {
    }
}
