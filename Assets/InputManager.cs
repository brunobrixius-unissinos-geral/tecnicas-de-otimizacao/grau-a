﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Mouse0))
        {
//            Debug.Log("MouseButtonDown");
            RaycastHit hit;
            UnityEngine.Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, 50))
//                print("Hit something!");

            // Does the ray intersect any objects excluding the player layer
            if (Physics.Raycast(transform.position, ray.direction, out hit, 50f))
            {
                Debug.DrawRay( Camera.main.ScreenToWorldPoint(Input.mousePosition), ray.direction * hit.distance, Color.yellow);
//                Debug.Log("Did Hit");
            }
            else
            {
                Debug.DrawRay(Camera.main.ScreenToWorldPoint(Input.mousePosition), ray.direction * 50, Color.white);
//                Debug.Log("Did not Hit");
            }
        }
    }
}
