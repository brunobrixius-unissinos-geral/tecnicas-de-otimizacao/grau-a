﻿
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
using System.IO;
using UnityEngine;

public class GenerateEnum
{
    [MenuItem("Tools/GenerateEnum")]
    public static void Go()
    {
        string enumName = "MyEnum";
        string folderName = "FolderName";

        GameObject[] gameObjects = Resources.LoadAll<GameObject>("Pool/");
        string[] enumEntries = new string[gameObjects.Length];

        for (int i = 0; i < gameObjects.Length; i++)
        {
            enumEntries[i] = gameObjects[i].name;
        }

        string filePathAndName = "Assets/Scripts/Enums/" + enumName + ".cs"; //The folder Scripts/Enums/ is expected to exist

        using (StreamWriter streamWriter = new StreamWriter(filePathAndName))
        {
            streamWriter.WriteLine($"namespace {folderName}");
            streamWriter.WriteLine("{");

            streamWriter.WriteLine("\tpublic enum " + enumName);
            streamWriter.WriteLine("\t{");
            for (int i = 0; i < enumEntries.Length; i++)
            {
                streamWriter.WriteLine("\t\t" + enumEntries[i] + ",");
            }
            streamWriter.WriteLine("\t}");
            streamWriter.WriteLine("}");
        }
        AssetDatabase.Refresh();
    }
}
#endif